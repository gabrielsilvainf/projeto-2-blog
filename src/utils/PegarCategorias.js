const GetCategorias = (setState) => {

    // Mock
    const _listaCateogiras = [
        {
            'id' : 1,
            'descricao': 'Montanha'
        },
        {
            'id' : 2,
            'descricao': 'Planície'
        },
        {
            'id' : 3,
            'descricao': 'Ilha'
        },
        {
            'id' : 4,
            'descricao': 'Pântano'
        }
    ]

    setState(_listaCateogiras)
}

export default GetCategorias