import React,  {useEffect, useState, useContext} from 'react'

import { useParams } from 'react-router-dom'
import getDetalhesPost from '../../../utils/getDetalhesPost'
import formatarData from '../../../utils/formatarData'
import TemaContext from '../../../contexts/TemaContext'

import pegarCategoriaPeloId from '../../../utils/pegarCategoriaPeloId'

const PaginaPost = () => {

    const { id } = useParams()
    const tema = useContext(TemaContext)

    const [postCarregado, setPostCarregado] = useState([])
    const [nomeCategoria, setNomeCategoria] = useState('')

    const style = {
        'texto': {
            'color': tema.fontColor,
            'margin': '15px auto',
            'width': '800px'
        },
        'imagem': {
            'margin': '0 auto',
            'display': 'block',
            'width': '800px'
        }
    }

    useEffect(async () => {
        const _postCarregado = await getDetalhesPost(id)
        setPostCarregado( _postCarregado )

        const _categoria = await pegarCategoriaPeloId( _postCarregado.idCategoria )
        setNomeCategoria(_categoria ? _categoria.descricao : null)
    },[])

    if(postCarregado){
        return (
            <>
            <h3 style={style.texto}>
                {postCarregado.id}-{postCarregado.idTipoPostagem === 1 ? "NOTÍCIA" : "ANÁLISE"}: {postCarregado.titulo}</h3>
            <h5 style={style.texto}>
                {nomeCategoria} - {formatarData(postCarregado.dataPostagem)} -
                Autor: {postCarregado.autor ? postCarregado.autor : "Gabriel"}
            </h5>
            {postCarregado.imagem ? 
                postCarregado.imagem.includes('http') ? <img  style={style.imagem} src={postCarregado.imagem}/>
                : null
            : null
            }
            
            <p style={style.texto}>{postCarregado.descricao}</p>
            </>
        )
    }


    return (
        <>
        <p>Carregando...</p>
        </>
    )
}

export default PaginaPost